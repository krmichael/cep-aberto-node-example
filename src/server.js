const app = require("./app");

app.listen(process.env.PORT || 3001, (error) => {
  if (error) {
    return console.log(`Error(s) found > ${error}`);
  }

  console.log("Server running...");
});
