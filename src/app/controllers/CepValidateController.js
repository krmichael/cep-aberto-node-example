const cepPromise = require("cep-promise");

class CepValidateController {
  async find(req, res) {
    const { cep } = req.body;

    try {
      cepPromise(cep)
        .then((data) => {
          return res.send({ isValid: true, data });
        })
        .catch(() => {
          return res.send({
            message: "Cep inválido",
          });
        });
    } catch (error) {
      return res.status(400).send({
        message: "Erro desconhecido, por favor tente novamente mais tarde!",
      });
    }
  }
}

module.exports = new CepValidateController();
