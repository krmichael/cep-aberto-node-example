const cluster = require("cluster");
const os = require("os");
const cpus = os.cpus();

cluster.isMaster
  ? (cpus.forEach(() => cluster.fork()),
    cluster.on("exit", () => {
      cluster.fork();
    }))
  : require("./server.js");
