const routes = require("express").Router();
const axios = require("axios").default;

const api = axios.create({
  baseURL: "https://www.cepaberto.com/api/v3",
  method: "GET",
  headers: {
    Authorization: "YOUR_TOKEN_HERE",
  },
});

routes.get("/", (req, res) => res.json({ message: "OK!" }));

routes.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,POST");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

routes.post("/cep", async (req, res) => {
  const { cep } = req.body;

  try {
    const response = await api.get(`/cep?cep=${cep}`);

    if (!Object.entries(response.data).length) {
      return res.send({
        message: "Cep inválido",
      });
    }

    return res.status(200).json({ address: response.data });
  } catch (error) {
    console.error("Error =>", error);
    return res.status(400).json({
      address: {},
      message: "Erro desconhecido, por favor tente novamente mais tarde!",
    });
  }
});

module.exports = routes;
